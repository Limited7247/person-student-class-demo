/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.swing.JOptionPane;

/**
 *
 * @author Limited
 */
public class Student extends Person {
    private String University;
    private String Major;   /// Khoa hoặc Chuyên ngành
    private int Year;       /// Năm sinh viên (1, 2, 3, ...)
    
    public Student()
    {
        super();   /// Khởi tạo Person
        this.University = "";
        this.Major = "";
        this.Year = 1;
    }

    public Student(Person person)
    {
        super(person);   /// Khởi tạo Person từ Person person
        this.University = "";
        this.Major = "";
        this.Year = 1;
    }
    
    public Student(String University, String Major, int Year) {
        super();
        this.University = University;
        this.Major = Major;
        this.Year = Year;
    }

    public Student(Person person, String University, String Major, int Year) {
        super(person);
        this.University = University;
        this.Major = Major;
        this.Year = Year;
    }
    
    public Student(String Name, int Age, String Hometown, String University, String Major, int Year) {
        super(Name, Age, Hometown);
        this.University = University;
        this.Major = Major;
        this.Year = Year;
    }
    
    @Override    /// Student.Show() kế thừa từ Person.Show(), thêm các thông tin mới
    public void Show()
    {
        super.Show();   /// Kế thừa Person.Show()
        System.out.println("Trường: " + this.University);
        System.out.println("Khoa: " + this.Major);
        System.out.println("Năm thứ: "+ this.Year);
        System.out.println("");
    }
    
    public void ShowDialog()
    {
        String strInfo = "";
        strInfo += "Họ tên: " + this.Name + "\n";
        strInfo += "Tuổi: " + this.Age + "\n";
        strInfo += "Quê quán: " + this.Hometown + "\n";
        strInfo += "Trường: " + this.University + "\n";
        strInfo += "Khoa: " + this.Major + "\n";
        strInfo += "Năm thứ: " + this.Year + "\n";
        
        JOptionPane.showMessageDialog(null, strInfo, "Thông tin Student", JOptionPane.INFORMATION_MESSAGE);
    }
    
    @Override   /// Student.Read() viết lại hoàn toàn, không kế thừa từ Person.Read()
    public void Read()
    {
        System.out.println("Nhập thông tin Student: ");
        if (JOptionPane.showConfirmDialog(null, "Vui lòng nhập các thông tin Student", "Nhập thông tin Student", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
        {
            this.Name = JOptionPane.showInputDialog(null, "Nhập Họ tên", "Nhập thông tin Student", JOptionPane.QUESTION_MESSAGE);
            this.Age = Integer.parseInt(JOptionPane.showInputDialog(null, "Nhập Tuổi", "Nhập thông tin Student", JOptionPane.QUESTION_MESSAGE));
            this.Hometown = JOptionPane.showInputDialog(null, "Nhập Quê quán", "Nhập thông tin Student", JOptionPane.QUESTION_MESSAGE);
        
            this.University = JOptionPane.showInputDialog(null, "Nhập Tên trường", "Nhập thông tin Student", JOptionPane.QUESTION_MESSAGE);
            this.Major = JOptionPane.showInputDialog(null, "Nhập Tên Khoa", "Nhập thông tin Student", JOptionPane.QUESTION_MESSAGE);
            this.Year = Integer.parseInt(JOptionPane.showInputDialog(null, "Nhập Năm sinh viên", "Nhập thông tin Student", JOptionPane.QUESTION_MESSAGE));
        }
       
        this.Show(); 
    }
}
