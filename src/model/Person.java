/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.swing.JOptionPane;

/**
 *
 * @author Limited
 */
public class Person {
    protected String Name;
    protected int Age;
    protected String Hometown;   /// Quê quán

    public Person() {
        this.Name = "";
        this.Age = 0;
        this.Hometown = "";
    }

    public Person(String Name, int Age, String Hometown) {
        this.Name = Name;
        this.Age = Age;
        this.Hometown = Hometown;
    }

    public Person(Person person) {
        this.Name = person.Name;
        this.Age = person.Age;
        this.Hometown = person.Hometown;
    }
    
    public void Set(String Name, int Age, String Hometown)
    {
        this.Name = Name;
        this.Age = Age;
        this.Hometown = Hometown;
    }
    
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getHometown() {
        return Hometown;
    }

    public void setHometown(String Hometown) {
        this.Hometown = Hometown;
    }
    
    public void Show()
    {
        System.out.println("Họ tên: " + this.Name);
        System.out.println("Tuổi: " + this.Age);
        System.out.println("Quê quán: " + this.Hometown);
    }
    
    /// Read Person via Dialog, Show after Read is done
    public void Read()
    {
        System.out.println("Nhập thông tin Person: ");
        JOptionPane.showMessageDialog(null, "Vui lòng nhập các thông tin Person", "Nhập thông tin Person", JOptionPane.INFORMATION_MESSAGE);
        this.Name = JOptionPane.showInputDialog(null, "Nhập Họ tên", "Nhập thông tin Person", JOptionPane.QUESTION_MESSAGE);
        this.Age = Integer.parseInt(JOptionPane.showInputDialog(null, "Nhập Tuổi", "Nhập thông tin Person", JOptionPane.QUESTION_MESSAGE));
        this.Hometown = JOptionPane.showInputDialog(null, "Nhập Quê quán", "Nhập thông tin Person", JOptionPane.QUESTION_MESSAGE);
        this.Show(); System.out.println("");
    }
}
